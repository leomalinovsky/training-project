'use strict';

const gulp = require('gulp');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');

gulp.task('default', function() {
	console.log('=== GULP ===');
});

gulp.task('hello', ['default'],  function(callback) {
	console.log('============>  hello gulp');
	callback();
});

gulp.task('uglify', function () {
	return gulp.src('./src/js/*.js')
			.pipe(uglify())
			.pipe(gulp.dest('./build/'));
})

gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});